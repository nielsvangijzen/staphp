<?php

namespace NielsVanGijzen\Staphp\Actions;

class SleepAction implements Action
{
    private int $maxSleepSeconds = 10;

    public function __construct(
        private int $sleepSeconds
    ) {
    }

    public function setMaxSleepSeconds(int $maxSleepSeconds)
    {
        $this->maxSleepSeconds = $maxSleepSeconds;
    }

    public function getSleepSeconds(): int
    {
        return $this->sleepSeconds;
    }

    public function act()
    {
        $sleepSeconds = $this->sleepSeconds;

        // Whenever the amount of sleep is lower than 0, we just
        // return
        if ($sleepSeconds <= 0) {
            return;
        }

        // We don't want the amount of sleep seconds to exceed the maximum
        // allowed amount.
        if ($sleepSeconds > $this->maxSleepSeconds) {
            $sleepSeconds = $this->maxSleepSeconds;
        }

        sleep($sleepSeconds);
    }
}
