<?php

namespace NielsVanGijzen\Staphp\Actions;

class EchoStringAction implements Action
{
    public function __construct(
        private string $text
    ){
    }

    public function act()
    {
        echo $this->text;
    }
}