<?php

namespace NielsVanGijzen\Staphp\Actions;

interface Action
{
    public function act();
}