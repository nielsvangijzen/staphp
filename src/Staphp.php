<?php

namespace NielsVanGijzen\Staphp;

use NielsVanGijzen\Staphp\Triggers\AbstractTrigger;
use NielsVanGijzen\Staphp\Triggers\EtcPasswdTrigger;
use NielsVanGijzen\Staphp\Triggers\SqlSleepTrigger;
use Symfony\Component\HttpFoundation\Request;

class Staphp
{
    private array $triggers = [
        SqlSleepTrigger::class,
        EtcPasswdTrigger::class,
    ];

    public function __construct(
        private Request $request
    ){
    }

    public static function createFromGlobals(): Staphp
    {
        return new Staphp(
            Request::createFromGlobals()
        );
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function handle()
    {
        foreach ($this->triggers as $trigger) {
            /** @var AbstractTrigger $triggerInstance */
            $triggerInstance = new $trigger($this->request);
            $action = $triggerInstance->getAction();

            if ($action !== false) {
                $action->act();
            }
        }
    }
}
