<?php

namespace NielsVanGijzen\Staphp\Triggers;

use NielsVanGijzen\Staphp\Actions\Action;
use NielsVanGijzen\Staphp\Actions\EchoStringAction;
use Symfony\Component\HttpFoundation\Request;

final class EtcPasswdTrigger extends AbstractTrigger
{
    private string $etcText;

    public function __construct(Request $request, $etcTextPath="/../Files/etc_passwd.txt")
    {
        parent::__construct($request);

        $this->openEtcPasswdFile($etcTextPath);
    }

    public function openEtcPasswdFile(string $etcTextPath)
    {
        $file = @file_get_contents(pathinfo(__FILE__, PATHINFO_DIRNAME) . $etcTextPath);
        if ($file === false) {
            $this->etcText = '<b>PHP Warning:</b>  fopen(): Failed to open stream: Permission denied';
        } else {
            $this->etcText = $file;
        }
    }

    public function getAction(): bool|Action
    {
        $inputs = $this->request->query->all();

        foreach ($inputs as $value) {
            if (str_contains($value, '/etc/passwd')) {
                return new EchoStringAction($this->etcText);
            }
        }

        return false;
    }
}