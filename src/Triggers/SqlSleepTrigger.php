<?php

namespace NielsVanGijzen\Staphp\Triggers;

use NielsVanGijzen\Staphp\Actions\Action;
use NielsVanGijzen\Staphp\Actions\SleepAction;

final class SqlSleepTrigger extends AbstractTrigger
{
    public function getAction(): bool|Action
    {
        $inputs = $this->request->query->all();

        foreach ($inputs as $value) {
            // We loop over all the inputs in order to get the intval
            $sleepValue = $this->parseSleepValue($value);
            if ($sleepValue !== false) {
                return new SleepAction($sleepValue);
            }
        }

        return false;
    }

    private function parseSleepValue(string $input): bool|int
    {
        // This regex will return 2 matches, one with the SLEEP(x)
        // and one with only x.
        preg_match_all("/SLEEP\((.*?)\)/", $input, $matches);

        if (count($matches[1]) < 1) {
            return false;
        }

        // We parse the integer value of the second entry.
        // This will be the integer only value.
        return intval($matches[1][0]);
    }
}