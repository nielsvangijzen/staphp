<?php

namespace NielsVanGijzen\Staphp\Triggers;

use NielsVanGijzen\Staphp\Actions\Action;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractTrigger
{
    public function __construct(
        protected Request $request
    ){
    }

    abstract public function getAction(): bool|Action;
}