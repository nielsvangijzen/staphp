<?php

namespace NielsVanGijzen\Staphp\Tests\Actions;

use NielsVanGijzen\Staphp\Actions\SleepAction;
use PHPUnit\Framework\TestCase;

class SleepActionTest extends TestCase
{
    public function testSleepAction()
    {
        $startTime = time();

        $action = new SleepAction(1);
        $action->act();

        $difference = time() - $startTime;

        $this->assertTrue($difference >= 1);
    }

    public function testSleepActionMaxSecondsExceeded()
    {
        $startTime = time();

        $action = new SleepAction(2);
        $action->setMaxSleepSeconds(1);
        $action->act();

        $difference = time() - $startTime;

        $this->assertTrue($difference >= 1);
        $this->assertTrue($difference < 2);
    }

    public function testSleepActionSleepSecondsNegative()
    {
        $startTime = time();

        $action = new SleepAction(-1);
        $action->act();

        $difference = time() - $startTime;

        $this->assertTrue($difference < 1);
    }
}