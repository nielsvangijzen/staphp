<?php

namespace NielsVanGijzen\Staphp\Tests;

use NielsVanGijzen\Staphp\Staphp;
use PHPUnit\Framework\TestCase;

class StaphpTest extends TestCase
{
    public function testHandle()
    {
        $_GET = ['password' => 'hunter123!'];
        $staphp = Staphp::createFromGlobals();

        $this->assertEquals(
            'hunter123!',
            $staphp->getRequest()->query->all()['password']
        );
    }

    public function testEchoStringActionThroughHandle()
    {
        $_GET = ['password' => '../../etc/passwd'];
        $staphp = Staphp::createFromGlobals();

        ob_start();
        $staphp->handle();
        $output = ob_get_clean();

        $this->assertStringContainsString('normaluser:x:1000:1000:User,,,:/home/user:/bin/bash', $output);
    }
}