<?php

namespace NielsVanGijzen\Staphp\Tests\Triggers;

use NielsVanGijzen\Staphp\Triggers\EtcPasswdTrigger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class EtcPasswdTriggerTest extends TestCase
{
    public function testEtcPasswdTriggerPathNotFound()
    {
        $trigger = new EtcPasswdTrigger(new Request(['/etc/passwd']), '../../../../../../../bijna_weekend');

        ob_start();
        $trigger->getAction()->act();
        $output = ob_get_clean();

        $this->assertEquals(
            '<b>PHP Warning:</b>  fopen(): Failed to open stream: Permission denied',
            $output,
        );
    }

    public function testEtcPasswdTriggerNoTrigger()
    {
        $trigger = new EtcPasswdTrigger(new Request(['/etc/d']));

        $this->assertFalse($trigger->getAction());
    }
}