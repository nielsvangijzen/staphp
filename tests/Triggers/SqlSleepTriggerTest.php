<?php

namespace NielsVanGijzen\Staphp\Tests\Triggers;

use NielsVanGijzen\Staphp\Actions\SleepAction;
use NielsVanGijzen\Staphp\Triggers\SqlSleepTrigger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class SqlSleepTriggerTest extends TestCase
{
    public function testSleepInRequest()
    {
        $request = new Request([
            'users' => '\' OR SLEEP(3) --'
        ]);

        $sqlTrigger = new SqlSleepTrigger($request);
        $action = $sqlTrigger->getAction();

        $this->assertInstanceOf(SleepAction::class, $action);
        $this->assertEquals(3, $action->getSleepSeconds());
    }

    public function testSleepInRequestNoSleep()
    {
        $request = new Request([
            'users' => '\' OR SLEP(3) --'
        ]);

        $sqlTrigger = new SqlSleepTrigger($request);
        $action = $sqlTrigger->getAction();

        $this->assertFalse($action);
    }
}